# devexpert-ansible

Repo dedicated to labbing Ansible for the Cisco DevNet Expert lab exam.

## Summary

This repo will be used 100% for Ansible labbing/testing. In the future, a CI/CD pipeline may be built to push and verify IOS/NX-OS configuration. The CI/CD pipeline is still TBD, but there's a good chance I'll map out the details soon.